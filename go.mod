module gitee.com/yysf_xin/go-common

go 1.20

require (
	gorm.io/driver/mysql v1.3.3
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.2
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.12 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
)

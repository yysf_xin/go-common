//go:build cgo

package db

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
)

func init() {
	RegDbType("sqlite", func(cfg *Config, config *gorm.Config) interface{} {
		if !cfg.Upgrade {
			_, err := os.Stat(cfg.DbName)
			if err != nil {
				if !os.IsExist(err) {
					cfg.Upgrade = true
				}
			}
			err = nil
		}
		return sqlite.Open(cfg.DbName)
	})
}

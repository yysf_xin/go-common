package db

import "gorm.io/gorm"

type Model struct {
	Id        uint `gorm:"primarykey"`
	CreatedAt Time `gorm:"type:time;<-:create"`
	UpdatedAt Time `gorm:"type:time"`
	DataFlag  int  `gorm:"default:1" json:"-"`
}

func (m *Model) GetId() uint {
	return m.Id
}

type State struct {
	Status int `gorm:"-"`
}

type Enable struct {
	Enabled bool
}

func (e *Enable) Update(db *gorm.DB) error {
	return db.Update("enabled", e.Enabled).Error
}

func (m *State) Change(status int) {
	m.Status = status
}

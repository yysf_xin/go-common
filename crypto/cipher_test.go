package crypto

import (
	"fmt"
	"log"
	"testing"
)

func TestCipher(t *testing.T) {
	c, err := NewAesCipher("a very very very very secret key") // 32

	if err != nil {
		panic(err)
	}

	plaintext := []byte("some really really really long plaintext")
	fmt.Printf("%s\n", plaintext)

	ciphertext, err := c.Encrypt(plaintext)

	if err != nil {
		panic(err)
	}
	log.Println(ciphertext)
	result, err := c.Decrypt(ciphertext)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", result)

	text, err := c.EncryptToBase64(plaintext)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(text)

	result, err = c.DecryptBase64(text)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(result))

	text, err = c.EncryptToBase64([]byte(""))
	if err != nil {
		log.Fatal(err)
	}
	log.Println(text)

	result, err = c.DecryptBase64(text)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(result))
}

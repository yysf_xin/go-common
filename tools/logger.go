package tools

import (
	"log"
	"os"
)

// Log global logger
var Log = log.New(os.Stdout, "", log.Lshortfile|log.LstdFlags)

func NewLog(prefix string) *log.Logger {
	return log.New(os.Stdout, prefix, log.Lshortfile|log.LstdFlags)
}

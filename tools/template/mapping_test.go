package template

import (
	"io"
	"os"
	"testing"
	"text/template"
)

type name2 struct {
	Name string
}
type name3 struct {
	Name string
}
type name struct {
	Name  string
	Name2 *name2
	Name3 name3
}

func TestMapObj1(t *testing.T) {
	testMethod(t, 1)
}

func testMethod(t *testing.T, method int) {
	n := &name{
		Name: "测试name",
		Name2: &name2{
			Name: "测试name2",
		},
		Name3: name3{
			Name: "测试name3",
		},
	}
	str := "test out ${Name} ${Name2.Name} ${Name3.Name}"
	if method == 1 {
		str = os.Expand(str, MapObj(n))
	}
	if t != nil {
		println(str)
	}
}

var tpl *template.Template

func TestTemplate(t *testing.T) {
	n := &name{
		Name: "测试name",
		Name2: &name2{
			Name: "测试name2",
		},
		Name3: name3{
			Name: "测试name3",
		},
	}

	str := "test out {{.Name}} {{.Name2.Name}} {{.Name3.Name}}"

	if tpl == nil {
		tpl, _ = template.New("test").Parse(str)
	}

	if t != nil {
		_ = tpl.Execute(os.Stdout, n)
	} else {
		_ = tpl.Execute(io.Discard, n)
	}
}

func BenchmarkTemplate(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		TestTemplate(nil)
	}
}

func BenchmarkMapObj1(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		TestMapObj1(nil)
	}
}
